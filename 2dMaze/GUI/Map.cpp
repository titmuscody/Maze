#include "Map.h"


Map::Map(string filePath)
{

	//create the vector map
	std::ifstream file;
	file.open(filePath);
	while (!file.eof()){
		vector<int> store;
		while (file.peek() != '\n'){

			int num;
			file >> num;
			if (file.fail())break;
			store.push_back(num);

		}

		file.ignore();
		map.push_back(store);
		store.clear();
		 
	}
	//create the tiles for the map
	canGo = al_create_bitmap(TILE_SIZE, TILE_SIZE);
	al_set_target_bitmap(canGo);
	al_clear_to_color(al_map_rgb(0, 255, 0));
	noGo = al_create_bitmap(TILE_SIZE, TILE_SIZE);
	al_set_target_bitmap(noGo);
	al_clear_to_color(al_map_rgb(255, 0, 0));
	level = al_create_bitmap(TILE_SIZE*map[0].size(), TILE_SIZE*map.size());
		//find and create players
	for (int i = 0; i < map.size(); i++){
		for (int j = 0; j < map[i].size(); j++){

			if (map[i][j] == 2){
				person.setX(j);
				person.setY(i);
				
			}

		}



	}
	updateBitmap();

}

void Map::updateBitmap(){

	al_set_target_bitmap(level);
	int TILE_SIZE = al_get_bitmap_width(person.getPlayer());
	for (int i = 0; i < map.size(); i++){
		for (int j = 0; j < map[i].size(); j++){
			if (map[i][j] == 1){
				al_draw_bitmap(noGo, j*TILE_SIZE, i * TILE_SIZE, 0);
			}
			else if (map[i][j] == 0){
				al_draw_bitmap(canGo, j * TILE_SIZE, i * TILE_SIZE, 0);
			}
			else if (map[i][j] == 2){
				al_draw_bitmap(canGo, j * TILE_SIZE, i * TILE_SIZE, 0);
				
			}

		}
		Point location = person.getLocation();
		al_draw_bitmap(person.getPlayer(), location.x * TILE_SIZE, location.y * TILE_SIZE, 0);

	}
}

void Map::movePlayer(Point newLocation){

	if (newLocation.x >= 0 && newLocation.x < map[0].size() && newLocation.y < map.size() && newLocation.y > 0){
		person.move(newLocation);
	}

	


	/*
		Point loc = person.getLocation();
	switch (){
		
	case 1:
		//left
		if (map[loc.y][loc.x - 1] == 1 || loc.x - 1 < 0){
			break;
		}
		else{
			loc.x -= 1;
			person.move(loc);
		}
		break;
	case 2:
		//up		
		if (map[loc.y-1][loc.x] == 1 || loc.y-1 < 0 ){
			break;
		}
		else{
			loc.y -= 1;
			person.move(loc);
		}
		break;
	case 3:
		//right
		if (loc.x + 1 > map[loc.y].size() || map[loc.y].at(loc.x + 1) == 1){
			break;
		}
		else{
			loc.x += 1;
			person.move(loc);
		}
		break;
	case 4:
		//down
		if (map[loc.y + 1][loc.x] == 1 || loc.y + 1 > map.size()){
			break;
		}
		else{
			loc.y += 1;
			person.move(loc);
		}
		break;

	}*/
	updateBitmap();
	
}

ALLEGRO_BITMAP* Map::getMap(){

	return level;
}

Point Map::getPlayerLocation(){
	return person.getLocation();
}

Map::~Map()
{


}
