#pragma once
#include "player.h"
#include <fstream>



class Map
{
public:

	Map(string);
	~Map();
	ALLEGRO_BITMAP* getMap();
	//send a 1 for left 2 for up 3 for right and 4 for down
	void movePlayer(Point);
	Point getPlayerLocation();
	

private:
	vector<vector<int>> map;
	ALLEGRO_BITMAP* level;
	void updateBitmap();
	player person;

	//bitmaps for drawing the map
	ALLEGRO_BITMAP* canGo;
	ALLEGRO_BITMAP* noGo;


};

